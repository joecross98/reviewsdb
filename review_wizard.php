<?php

	require_once('preheader.php'); // <-- this include file MUST go first before any HTML/output

	#the code for the class
	include ('reviewsCRUDclass.php'); // <-- this include file MUST go first before any HTML/output

    #this one line of code is how you implement the class
    ########################################################
    ##

   function formatAsCurrency($data){
        setlocale(LC_MONETARY, 'en_US');
        return money_format('%.0n', $data);
        //return number_format($data, 0, '.', ','); //displays 10 as 10.00
    }
?>


<?php
    
    $tblDemo3 = new ajaxCRUD("Review", "reviews", "idReviews");
    $tblDemo3->defineRelationship("PrinterID", "printers", "idPrinter", "CONCAT(Manufacturer, \" \", Model)");
    $tblDemo3->omitFieldCompletely("idReviews");
    $tblDemo3->omitFieldCompletely("Created");
    $tblDemo3->displayAs("PrinterID", "Printer");
    $tblDemo3->displayAs("ColorCode", "Success?");
    $tblDemo3->displayAs("Commentor", "Your Name");
    //$tblDemo3->disallowDelete();
    $allowableValues = array("Unsuccessful", "Successful with hard provisions", "Successful with easy provisions", "Successfully used.  Well-suited to beginners.");
    $tblDemo3->defineAllowableValues("ColorCode", $allowableValues);
    $tblDemo3->setTextareaHeight('Notes', 50);
    $tblDemo3->formatFieldWithFunction('Price', 'formatAsCurrency');
    $tblDemo3->setTextboxWidth('Notes', 400);
    $tblDemo3->setTextareaHeight('Advantages', 50);
    $tblDemo3->setTextboxWidth('Advantages', 400);
    $tblDemo3->setTextareaHeight('Disadvantages', 50);
    $tblDemo3->setTextboxWidth('Disadvantages', 400);
    $tblDemo3->addValueOnInsert("Created", "NOW()");
    //$tblDemo3->setOrientation("vertical");
    //$tblDemo3->addOrderBy("ORDER BY PrinterID ASC ");
    //$tblDemo3->addWhereClause("WHERE idReviews = NULL");
    $tblDemo3->displayAddFormTop();
    //$tblDemo3->disallowAdd();
    $tblDemo3->addTableBorder();
    $tblDemo3->turnOffAjaxADD();
    $tblDemo3->showTable();
    
?>

<script type="text/javascript">$('#add_form_reviews').slideDown('fast'); x = document.getElementById('add_form_reviews'); t = setTimeout('x.scrollIntoView(false)', 200);</script>
			