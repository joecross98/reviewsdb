<?php

	require_once('preheader.php'); // <-- this include file MUST go first before any HTML/output

	#the code for the class
	include ('ajaxCRUD.class.php'); // <-- this include file MUST go first before any HTML/output

    #this one line of code is how you implement the class
    ########################################################
    ##

   function formatAsCurrency($data){
        setlocale(LC_MONETARY, 'en_US');
        return money_format('%.0n', $data);
        //return number_format($data, 0, '.', ','); //displays 10 as 10.00
    }
?>

<table cellpadding=20 width=100%>
<tr>
<td align="center"><h2>3D Printers</td>
<td align="center"><h2>Hands</td>
<td align="center"><h2>Reviews</td>
<td align="center"><h2>Settings</td>
<td align="center"><h2>Materials</td>
<td align="center"><h2>Supported Materials</td>
</tr>
<tr valign="top">
<td>

<?php
    $tblDemo = new ajaxCRUD("Printer", "printers", "idPrinter");
    $tblDemo->omitFieldCompletely("idPrinter");
    //$tblDemo->disallowDelete();
    $tblDemo->addOrderBy("ORDER BY Manufacturer, Model ASC ");
    $tblDemo->showTable();

?>

</td>
<td>

<?php

    $tblDemo2 = new ajaxCRUD("Hand", "hands", "idHands");
    $tblDemo2->omitFieldCompletely("idHands");
    //$tblDemo2->disallowDelete();
    $tblDemo2->defineCheckbox("TaskSpecific");
    $tblDemo2->displayAs("TaskSpecific", "Task Specific?");
    $tblDemo2->addOrderBy("ORDER BY TaskSpecific, Name ASC ");
    $tblDemo2->showTable();
    
?>

</td>
<td>

<?php
    
    $tblDemo3 = new ajaxCRUD("Review", "reviews", "idReviews");
    $tblDemo3->defineRelationship("PrinterID", "printers", "idPrinter", "CONCAT(Manufacturer, \" \", Model)");
    $tblDemo3->omitFieldCompletely("idReviews");
    $tblDemo3->displayAs("PrinterID", "Printer");
    $tblDemo3->displayAs("ColorCode", "Success?");
    //$tblDemo3->disallowDelete();
    $allowableValues = array("Unsuccessful", "Successful with hard provisions", "Successful with easy provisions", "Successfully used.  Well-suited to beginners.");
    $tblDemo3->defineAllowableValues("ColorCode", $allowableValues);
    $tblDemo3->setTextareaHeight('Notes', 50);
    $tblDemo3->formatFieldWithFunction('Price', 'formatAsCurrency');
    $tblDemo3->setTextboxWidth('Notes', 400);
    $tblDemo3->setTextareaHeight('Advantages', 50);
    $tblDemo3->setTextboxWidth('Advantages', 400);
    $tblDemo3->setTextareaHeight('Disadvantages', 50);
    $tblDemo3->setTextboxWidth('Disadvantages', 400);
    $tblDemo3->setOrientation("vertical");
    $tblDemo3->addOrderBy("ORDER BY PrinterID ASC ");
    $tblDemo3->showTable();
    
?>
	
</td>
<td>

<?php
    
    $tblDemo4 = new ajaxCRUD("Preferred Setting", "settings", "idSettings");
    $tblDemo4->defineRelationship("PrinterID", "printers", "idPrinter", "CONCAT(Manufacturer, \" \", Model)");
    $tblDemo4->defineRelationship("HandID", "hands", "idHands", "Name");
    $tblDemo4->displayAs("PrinterID", "Printer");
    $tblDemo4->displayAs("HandID", "Hand");
    $tblDemo4->displayAs("LayerHeight", "Layer Height (mm)");
    $tblDemo4->displayAs("FillDensity", "Fill Density (%)");
    $tblDemo4->displayAs("PrinterSpeed", "Printer Speed (mm/s)");
    $tblDemo4->displayAs("AdhesionType", "Adhesion Type");
    $tblDemo4->displayAs("ShellThickness", "Shell Thickness (mm)");
    $tblDemo4->displayAs("EnableRetraction", "Enable Retraction");
    $tblDemo4->defineCheckbox("EnableRetraction");
    $tblDemo4->displayAs("BottomTopThickness", "Bottom/Top Thickness (mm)");
    $tblDemo4->displayAs("SupportType", "Support Type");
    $tblDemo4->omitFieldCompletely("idSettings");
    $tblDemo4->setFileUpload("Photo", "Pics/", "Pics/");
    //$tblDemo4->disallowDelete();
    $tblDemo4->setOrientation("vertical");
    $tblDemo4->addOrderBy("ORDER BY PrinterID ASC ");
    $tblDemo4->showTable();

?>
	
</td>
<td>

<?php
    $tblDemo5 = new ajaxCRUD("Material", "materials", "idMaterial");
    $tblDemo5->omitFieldCompletely("idMaterial");
    //$tblDemo5->disallowDelete();
    //$tblDemo5->addOrderBy("ORDER BY Manufacturer, Model ASC ");
    $tblDemo5->displayAs("type", "Type");
    $tblDemo5->displayAs("diameter", "Diameter");
    $tblDemo5->showTable();

?>

</td>
<td>

<?php
    $tblDemo6 = new ajaxCRUD("Supported Materials", "printer_material_rel", "idMaterial");
    $tblDemo6->defineRelationship("printer", "printers", "idPrinter", "CONCAT(Manufacturer, \" \", Model)");
    $tblDemo6->defineRelationship("material", "materials", "idMaterial", "CONCAT(Type, \" \", Diameter)");
    //$tblDemo6->disallowDelete();
    //$tblDemo6->addOrderBy("ORDER BY Manufacturer, Model ASC ");
    $tblDemo6->showTable();

?>

</tr>
</table>
		