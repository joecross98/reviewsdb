<html lang="en-US"><head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>Printer Review and Settings Database | E-nabling The Future</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://enablingthefuture.org/xmlrpc.php">
<!--[if lt IE 9]>
<script src="http://enablingthefuture.org/wp-content/themes/yoko-wpcom/js/html5.js" type="text/javascript"></script>
<script src="http://enablingthefuture.org/wp-content/themes/yoko-wpcom/js/css3-mediaqueries.js" type="text/javascript"></script>
<![endif]-->
<link rel="alternate" type="application/rss+xml" title="E-nabling The Future » Feed" href="http://enablingthefuture.org/feed/">
<link rel="alternate" type="application/rss+xml" title="E-nabling The Future » Comments Feed" href="http://enablingthefuture.org/comments/feed/">
<link rel="alternate" type="application/rss+xml" title="E-nabling The Future » GET INVOLVED! Comments Feed" href="http://enablingthefuture.org/get-involved/feed/">
<link rel="stylesheet" id="style-css" href="http://enablingthefuture.org/wp-content/themes/yoko-wpcom/style.css?ver=4.0.1" type="text/css" media="all">
<link rel="stylesheet" id="yoko-fonts-css" href="http://fonts.googleapis.com/css?family=Droid+Sans%3Aregular%2Cbold%7CDroid+Serif%3Aregular%2Citalic%2Cbold%2Cbolditalic&amp;subset=latin&amp;ver=4.0.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_image_widget-css" href="http://enablingthefuture.org/wp-content/plugins/jetpack/modules/widgets/image-widget/style.css?ver=20140808" type="text/css" media="all">
<link rel="stylesheet" id="infinity-yoko-css" href="http://enablingthefuture.org/wp-content/plugins/home4/enabler/public_html/wp-content/themes/yoko-wpcom/yoko.css?ver=20120227" type="text/css" media="all">
<link rel="stylesheet" id="genericons-css" href="http://enablingthefuture.org/wp-content/plugins/jetpack/_inc/genericons/genericons/genericons.css?ver=3.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="http://enablingthefuture.org/wp-content/plugins/jetpack/css/jetpack.css?ver=3.3" type="text/css" media="all">
<script id="twitter-wjs" src="http://platform.twitter.com/widgets.js"></script><script type="text/javascript" src="http://enablingthefuture.org/wp-includes/js/jquery/jquery.js?ver=1.11.1"></script><style type="text/css"></style>
<script type="text/javascript" src="http://enablingthefuture.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/themes/yoko-wpcom/js/smoothscroll.js?ver=1.0"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://enablingthefuture.org/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://enablingthefuture.org/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.0.1">
<link rel="canonical" href="http://enablingthefuture.org/get-involved/">
<link rel="shortlink" href="http://wp.me/P5qDfS-dd">
<style type="text/css">img#wpstats{display:none}</style>
<!-- Jetpack Open Graph Tags -->
<meta property="og:type" content="article">
<meta property="og:title" content="Printer Review and Settings Database">
<meta property="og:url" content="http://enablingthefuture.org/get-involved/">
<meta property="og:description" content="Join the Community The first step is to decide how you'd like to be involved in e-NABLE. &nbsp;Members can do everything from research and development, fabrication, operations coordination, media relati...">
<meta property="article:published_time" content="2014-05-26T17:53:39+00:00">
<meta property="article:modified_time" content="2014-12-04T04:34:17+00:00">
<meta property="article:author" content="http://enablingthefuture.org/author/a1n1c0/">
<meta property="og:site_name" content="E-nabling The Future">
<meta property="og:image" content="http://enablingthefuture.org/wp-content/uploads/2014/01/enablemapnewest.jpg">
<meta name="twitter:site" content="@jetpack">
<meta name="twitter:image" content="http://enablingthefuture.org/wp-content/uploads/2014/01/enablemapnewest.jpg?w=240">
<meta name="twitter:card" content="summary">
<link rel="stylesheet" type="text/css" id="gravatar-card-css" href="http://s.gravatar.com/css/hovercard.css?ver=2014Decaa"><link rel="stylesheet" type="text/css" id="gravatar-card-services-css" href="http://s.gravatar.com/css/services.css?ver=2014Decaa"></head>
<body class="page page-id-819 page-template-default group-blog" data-twttr-rendered="true">
<div id="page" class="clearfix">
	<header id="branding">
		<nav id="mainnav" class="clearfix" role="navigation">
			<button class="menu-toggle">Menu</button>
			<div class="menu-rex-1-0-container"><ul id="menu-rex-1-0" class="menu nav-menu"><li id="menu-item-841" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-841"><a href="http://enablingthefuture.org/">HOME</a></li>
<li id="menu-item-842" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-842"><a href="http://enablingthefuture.org/about/">ABOUT</a>
<ul class="sub-menu">
	<li id="menu-item-846" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-846"><a href="http://enablingthefuture.org/faqs/">FAQS</a></li>
	<li id="menu-item-843" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-843"><a href="http://enablingthefuture.org/contact-us/">CONTACT</a></li>
	<li id="menu-item-874" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-874"><a href="http://enablingthefuture.org/our-tree/">OUR GIVING TREE</a></li>
</ul>
</li>
<li id="menu-item-850" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-819 current_page_item menu-item-850"><a href="http://enablingthefuture.org/get-involved/">GET INVOLVED!</a></li>
<li id="menu-item-840" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-840"><a href="http://enablingthefuture.org/resources/">RESOURCES</a>
<ul class="sub-menu">
	<li id="menu-item-1286" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1286"><a href="http://enablingthefuture.org/handomatic/">Handomatic</a></li>
	<li id="menu-item-865" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-865"><a href="http://enablingthefuture.org/resources/hardware-sources/">Hardware Sources</a></li>
	<li id="menu-item-864" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-864"><a href="http://enablingthefuture.org/resources/tips-for-successful-prints/">Tips for Successful Prints</a></li>
	<li id="menu-item-866" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-866"><a href="http://enablingthefuture.org/resources/general-assembly-tips/">Assembly Tips and Tricks</a></li>
	<li id="menu-item-863" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-863"><a href="http://enablingthefuture.org/resources/getting-started-in-fusion-360/">Getting Started in Fusion 360</a></li>
	<li id="menu-item-893" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-893"><a href="http://enablingthefuture.org/resources/creating-a-hand-casting/">Creating a Hand Casting</a></li>
	<li id="menu-item-2070" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2070"><a href="http://enablingthefuture.org/videos-prosthetists-meets-printers-conference-2014/">Videos  Prosthetists Meets Printers Conference 2014</a></li>
	<li id="menu-item-903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-903"><a href="http://enablingthefuture.org/resources/support-for-upper-limb-differences/">Upper Limb Differences  Support</a></li>
</ul>
</li>
<li id="menu-item-867" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-867"><a href="http://enablingthefuture.org/upper-limb-prosthetics/">PROSTHETIC DEVICES</a>
<ul class="sub-menu">
	<li id="menu-item-1447" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1447"><a href="http://enablingthefuture.org/upper-limb-prosthetics/the-raptor-hand/">The Raptor Hand</a></li>
	<li id="menu-item-873" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-873"><a href="http://enablingthefuture.org/upper-limb-prosthetics/cyborg-beast/">The Cyborg Beast</a></li>
	<li id="menu-item-872" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-872"><a href="http://enablingthefuture.org/upper-limb-prosthetics/talon-hand/">Talon Hand</a></li>
	<li id="menu-item-870" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-870"><a href="http://enablingthefuture.org/upper-limb-prosthetics/odysseus-hand/">The Odysseus Hand</a></li>
	<li id="menu-item-912" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912"><a href="http://enablingthefuture.org/upper-limb-prosthetics/second-degree/">The Second Degree Hand</a></li>
	<li id="menu-item-868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-868"><a href="http://enablingthefuture.org/upper-limb-prosthetics/the-owen-replacement-finger/">The Owen Replacement Finger</a></li>
	<li id="menu-item-871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-871"><a href="http://enablingthefuture.org/upper-limb-prosthetics/additional-gauntlet-options/">Additional Gauntlet Options</a></li>
	<li id="menu-item-1153" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1153"><a href="http://enablingthefuture.org/upper-limb-prosthetics/the-limbitless-arm/">The Limbitless Arm</a></li>
	<li id="menu-item-1906" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1906"><a href="http://enablingthefuture.org/upper-limb-prosthetics/rit-arm/">The RIT Arm</a></li>
</ul>
</li>
<li id="menu-item-845" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-845"><a href="http://enablingthefuture.org/e-nable-event-calendar/">EVENTS</a></li>
<li id="menu-item-914" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-914"><a href="http://enablingthefuture.org/build-a-hand/safety-guidelines/">SAFETY GUIDELINES</a></li>
<li id="menu-item-919" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-919"><a href="http://enablingthefuture.org/media-coverage/">MEDIA COVERAGE</a></li>
<li id="menu-item-1180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1180"><a href="http://enablingthefuture.org/press-info/">PRESS INFO</a>
<ul class="sub-menu">
	<li id="menu-item-1181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1181"><a href="http://enablingthefuture.org/prosthetists-meet-3d-printers-press-release/">Prosthetists Meet 3D Printers  Press Release</a></li>
</ul>
</li>
<li id="menu-item-2088" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2088"><a href="http://enablingthefuture.org/rdonate/">DONATIONS</a></li>
<li id="menu-item-1366" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1366"><a href="https://www.facebook.com/enableorganization">FACEBOOK</a></li>
<li id="menu-item-1367" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1367"><a href="https://twitter.com/Enablethefuture">TWITTER</a></li>
<li id="menu-item-1726" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1726"><a href="http://enablingthefuture.org/photo-galleries/">PHOTO GALLERIES</a>
<ul class="sub-menu">
	<li id="menu-item-1727" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1727"><a href="http://enablingthefuture.org/e-nable-prosthetists-meet-printers-event-album/">e-NABLE Prosthetists Meet Printers Event Album</a></li>
</ul>
</li>
</ul></div>		</nav><!-- end mainnav -->

		<hgroup id="site-title">
						<h1><a href="http://enablingthefuture.org/" title="E-nabling The Future">E-nabling The Future</a></h1>
			<h2 id="site-description">A network of passionate volunteers using 3D printing to give the World a "Helping Hand."</h2>
		</hgroup><!-- end site-title -->

					<img src="http://enablingthefuture.org/wp-content/uploads/2014/12/cropped-slider6.jpg" class="headerimage" width="1102" height="350>" alt="">
				<div class="clear"></div>

			</header><!-- end header -->
<div id="wrap">
<div id="main" class="one-sidebar">

	<div id="content" class="site-content">
				
<article id="post-819" class="post-819 page type-page status-publish hentry">

	<header class="page-entry-header">
		<h1 class="entry-title">Printer Review and Settings Database</h1>
	</header><!--end page-entry-hader-->

	<div class="single-entry-content">
		
<h1></h1>

<h1>Click <a href="review_wizard.php"><strong>here</strong></a> if you would like to log a printer review<br></h1>

<h1>Click <a href="review_table.php"><strong>here</strong></a> if you would like to view or search existing reviews<br></h1>


<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="sharedaddy sd-sharing-enabled"><div class="robots-nocontent sd-block sd-social sd-social-icon sd-sharing"><h3 class="sd-title">Share this:</h3><div class="sd-content"><ul><li class="share-twitter"><a rel="nofollow" data-shared="sharing-twitter-819" class="share-twitter sd-button share-icon no-text" href="http://enablingthefuture.org/get-involved/?share=twitter&amp;nb=1" title="Click to share on Twitter"><span><span class="share-count">9</span></span><span class="sharing-screen-reader-text">Click to share on Twitter<span class="share-count">9</span></span></a></li><li class="share-facebook"><a rel="nofollow" data-shared="sharing-facebook-819" class="share-facebook sd-button share-icon no-text" href="http://enablingthefuture.org/get-involved/?share=facebook&amp;nb=1" title="Share on Facebook"><span><span class="share-count">116</span></span><span class="sharing-screen-reader-text">Share on Facebook<span class="share-count">116</span></span></a></li><li class="share-google-plus-1"><a rel="nofollow" data-shared="sharing-google-819" class="share-google-plus-1 sd-button share-icon no-text" href="http://enablingthefuture.org/get-involved/?share=google-plus-1&amp;nb=1" title="Click to share on Google+"><span></span><span class="sharing-screen-reader-text">Click to share on Google+</span></a></li><li class="share-pinterest"><a rel="nofollow" data-shared="" class="share-pinterest sd-button share-icon no-text" href="http://enablingthefuture.org/get-involved/?share=pinterest&amp;nb=1" title="Click to share on Pinterest"><span></span><span class="sharing-screen-reader-text">Click to share on Pinterest</span></a></li><li class="share-reddit"><a rel="nofollow" data-shared="" class="share-reddit sd-button share-icon no-text" href="http://enablingthefuture.org/get-involved/?share=reddit&amp;nb=1" title="Click to share on Reddit"><span></span><span class="sharing-screen-reader-text">Click to share on Reddit</span></a></li><li class="share-email share-service-visible"><a rel="nofollow" data-shared="" class="share-email sd-button share-icon no-text" href="http://enablingthefuture.org/get-involved/?share=email&amp;nb=1" title="Click to email this to a friend"><span></span><span class="sharing-screen-reader-text">Click to email this to a friend</span></a></li><li class="share-end"></li></ul></div></div></div>		<div class="clear"></div>
					</div><!--end entry-content-->

</article>		
	</div><!-- end content -->


	<div id="secondary" class="widget-area" role="complementary">
					</div><!-- #secondary .widget-area -->
</div><!-- end main -->

<div id="tertiary" class="widget-area" role="complementary">
	<aside id="image-2" class="widget widget_image"><h3 class="widget-title">Donate</h3><div class="jetpack-image-container"><a href="http://donate.e-nable.me/"><img src="http://enablingthefuture.org/wp-content/uploads/2014/04/donatebutton.jpg" class="alignnone" width="262"></a></div>
</aside><aside id="blog_subscription-2" class="widget jetpack_subscription_widget"><h3 class="widget-title"><label for="subscribe-field">Follow Blog via Email</label></h3>
		<form action="#" method="post" accept-charset="utf-8" id="subscribe-blog-blog_subscription-2">
			<div id="subscribe-text"><p>Enter your email address to follow this blog and receive notifications of new posts by email.</p>
</div><p>Join 672 other subscribers</p>

			<p id="subscribe-email">
				<label id="jetpack-subscribe-label" for="subscribe-field" style="clip: rect(1px 1px 1px 1px); position: absolute; height: 1px; width: 1px; overflow: hidden;">
					Email Address				</label>
				<input type="email" name="email" value="Email Address" id="subscribe-field" placeholder="Email Address">
			</p>

			<p id="subscribe-submit">
				<input type="hidden" name="action" value="subscribe">
				<input type="hidden" name="source" value="http://enablingthefuture.org/get-involved/">
				<input type="hidden" name="sub-type" value="widget">
				<input type="hidden" name="redirect_fragment" value="blog_subscription-2">
								<input type="submit" value="FOLLOW OUR BLOG" name="jetpack_subscriptions_widget">
			</p>
		</form>

		<script>
			( function( d ) {
				if ( ( 'placeholder' in d.createElement( 'input' ) ) ) {
					var label = d.getElementById( 'jetpack-subscribe-label' );
 					label.style.clip 	 = 'rect(1px, 1px, 1px, 1px)';
 					label.style.position = 'absolute';
 					label.style.height   = '1px';
 					label.style.width    = '1px';
 					label.style.overflow = 'hidden';
				}
			} ) ( document );
		</script>

		
</aside><aside id="search-2" class="widget widget_search"><h3 class="widget-title">SEARCH</h3>
<form role="search" method="get" class="searchform" action="http://enablingthefuture.org/">
	<div>
		<label class="screen-reader-text" for="s">Search for:</label>
		<input type="text" class="search-input" value="" name="s" id="s">
		<input type="submit" class="searchsubmit" value="Search">
	</div>
</form></aside><aside id="facebook-likebox-2" class="widget widget_facebook_likebox"><h3 class="widget-title"><a href="https://www.facebook.com/enableorganization">E-Nable FACEBOOK</a></h3><iframe src="http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fenableorganization&amp;width=200&amp;height=110&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;show_border=true&amp;header=false&amp;force_wall=false" scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 200px;  height: 110px; background: #fff"></iframe></aside><aside id="twitter_timeline-2" class="widget widget_twitter_timeline"><h3 class="widget-title">FOLLOW US ON TWITTER</h3><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-timeline twitter-timeline-rendered" allowfullscreen="" style="border: none; max-width: 100%; min-width: 180px; width: 225px;" title="Twitter Timeline" height="383"></iframe></aside>		<aside id="recent-posts-2" class="widget widget_recent_entries">		<h3 class="widget-title">RECENT POSTS</h3>		<ul>
					<li>
				<a href="http://enablingthefuture.org/2014/12/18/raptor-reloaded-design-and-intent/">Raptor Reloaded:  Design and Intent</a>
						</li>
					<li>
				<a href="http://enablingthefuture.org/2014/12/04/e-nable-%e2%80%a2-2014-winner-in-nominent-trust-100/">e-NABLE  2014 Winner In Nominet Trust 100!</a>
						</li>
					<li>
				<a href="http://enablingthefuture.org/2014/11/30/hands-across-the-world-%e2%80%a2-calling-all-makers/">Hands Across The World  Calling All Makers!!</a>
						</li>
					<li>
				<a href="http://enablingthefuture.org/2014/11/27/thank-you-liam/">Thank You Liam</a>
						</li>
					<li>
				<a href="http://enablingthefuture.org/2014/11/18/hands-across-borders-%e2%80%a2-scout-troops-make-3d-printed-hands/">Hands Across Borders  Scout Troops Make 3D Printed Hands</a>
						</li>
				</ul>
		</aside><aside id="categories-2" class="widget widget_categories"><h3 class="widget-title">CATEGORIES</h3>		<ul>
	<li class="cat-item cat-item-307677970"><a href="http://enablingthefuture.org/category/awards/">Awards</a>
</li>
	<li class="cat-item cat-item-11354137"><a href="http://enablingthefuture.org/category/design-improvements/">Design improvements</a>
</li>
	<li class="cat-item cat-item-211879329"><a href="http://enablingthefuture.org/category/e-nable-google/">E-Nable GOOGLE+</a>
</li>
	<li class="cat-item cat-item-37768760"><a href="http://enablingthefuture.org/category/events-and-media/">Events and Media</a>
</li>
	<li class="cat-item cat-item-1577959"><a href="http://enablingthefuture.org/category/feel-good-stories/">Feel Good Stories</a>
</li>
	<li class="cat-item cat-item-547342"><a href="http://enablingthefuture.org/category/giving-back/">Giving Back</a>
</li>
	<li class="cat-item cat-item-208351537"><a href="http://enablingthefuture.org/category/hands-created/">Hands Created</a>
</li>
	<li class="cat-item cat-item-110"><a href="http://enablingthefuture.org/category/in-the-news/">In the News</a>
</li>
	<li class="cat-item cat-item-21133"><a href="http://enablingthefuture.org/category/informational/">Informational</a>
</li>
	<li class="cat-item cat-item-34537"><a href="http://enablingthefuture.org/category/personal-stories/">Personal stories</a>
</li>
	<li class="cat-item cat-item-285906"><a href="http://enablingthefuture.org/category/research-and-development/">Research and Development</a>
</li>
	<li class="cat-item cat-item-52816"><a href="http://enablingthefuture.org/category/spreading-the-word/">Spreading the Word</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://enablingthefuture.org/category/uncategorized/">Uncategorized</a>
</li>
		</ul>
</aside><aside id="archives-2" class="widget widget_archive"><h3 class="widget-title">ARCHIVES</h3>		<ul>
	<li><a href="http://enablingthefuture.org/2014/12/">December 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/11/">November 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/10/">October 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/09/">September 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/08/">August 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/07/">July 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/06/">June 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/05/">May 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/04/">April 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/03/">March 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/02/">February 2014</a></li>
	<li><a href="http://enablingthefuture.org/2014/01/">January 2014</a></li>
		</ul>
</aside></div><!-- end tertiary .widget-area -->
	</div><!-- end wrap -->

	<footer id="colophon" class="clearfix">
		<p><a href="http://wordpress.org/" rel="generator">Proudly powered by WordPress</a><span class="sep"> | </span>Theme: Yoko by <a href="http://www.elmastudio.de/wordpress-themes/">Elmastudio</a>.</p>
		<a href="#page" class="top">Top</a>
	</footer><!-- end colophon -->
	<div style="display:none">
	<div class="grofile-hash-map-2c5d82e6ae3191a2deeb1385cd23b6df">
	</div>
	<div class="grofile-hash-map-b5bf511bb19971667b016075f0ba9331">
	</div>
	<div class="grofile-hash-map-36609bfa16d64c79baa6128654dff596">
	</div>
	<div class="grofile-hash-map-92c0cfc646ab843185e2ac0d467076fd">
	</div>
	<div class="grofile-hash-map-77135418c5959e91caa00398a5d787ef">
	</div>
	<div class="grofile-hash-map-5b2cccef859b4ee6c8a3ad1f838c1c32">
	</div>
	<div class="grofile-hash-map-d9b267947e545ddbcf445311f7e83a6f">
	</div>
	<div class="grofile-hash-map-09d922a4fa99675fbf14ffbbccda2faa">
	</div>
	<div class="grofile-hash-map-7be2cd75e734fe2ab8d39ffc5d4a367b">
	</div>
	<div class="grofile-hash-map-7c3f4f12ac594e2f90b5bcfa453bffdb">
	</div>
	<div class="grofile-hash-map-d1fa4b43dac7fe27b5e9db876448a01e">
	</div>
	<div class="grofile-hash-map-79dc6d94cc14463a646f39f372d76b31">
	</div>
	<div class="grofile-hash-map-a57e76a4f343c5df5855df6fc05a3523">
	</div>
	<div class="grofile-hash-map-4db4f5795470587570c6c68748de8981">
	</div>
	<div class="grofile-hash-map-3d15279baf57fd026829bc33cad2b503">
	</div>
	<div class="grofile-hash-map-6e02d0a673c9e09acaea892c55f937a8">
	</div>
	<div class="grofile-hash-map-a86e031772460723feb08b8fd35d5369">
	</div>
	<div class="grofile-hash-map-b5bf511bb19971667b016075f0ba9331">
	</div>
	<div class="grofile-hash-map-dcc12879526609fa5bc7a257b8f60234">
	</div>
	</div>
		<script type="text/javascript">
			!function(d,s,id){
				var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
				if(!d.getElementById(id)){
					js=d.createElement(s);
					js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
					fjs.parentNode.insertBefore(js,fjs);
				}
			}(document,"script","twitter-wjs");
		</script>
		
	<script type="text/javascript">
		WPCOM_sharing_counts = {"http:\/\/enablingthefuture.org\/get-involved\/":819}	</script>
		<script type="text/javascript">
		jQuery(document).on( 'ready post-load', function(){
			jQuery( 'a.share-twitter' ).on( 'click', function() {
				window.open( jQuery(this).attr( 'href' ), 'wpcomtwitter', 'menubar=1,resizable=1,width=600,height=350' );
				return false;
			});
		});
		</script>
				<script type="text/javascript">
		jQuery(document).on( 'ready post-load', function(){
			jQuery( 'a.share-facebook' ).on( 'click', function() {
				window.open( jQuery(this).attr( 'href' ), 'wpcomfacebook', 'menubar=1,resizable=1,width=600,height=400' );
				return false;
			});
		});
		</script>
				<script type="text/javascript">
		jQuery(document).on( 'ready post-load', function(){
			jQuery( 'a.share-google-plus-1' ).on( 'click', function() {
				window.open( jQuery(this).attr( 'href' ), 'wpcomgoogle-plus-1', 'menubar=1,resizable=1,width=480,height=550' );
				return false;
			});
		});
		</script>
							<script type="text/javascript">
			jQuery(document).on('ready', function(){
				jQuery('body').on('click', 'a.share-pinterest', function(e){
					e.preventDefault();

					// Load Pinterest Bookmarklet code
					var s = document.createElement("script");
					s.type = "text/javascript";
					s.src = window.location.protocol + "//assets.pinterest.com/js/pinmarklet.js?r=" + ( Math.random() * 99999999 );
					var x = document.getElementsByTagName("script")[0];
					x.parentNode.insertBefore(s, x);

					// Trigger Stats
					var s = document.createElement("script");
					s.type = "text/javascript";
					s.src = this + ( this.toString().indexOf( '?' ) ? '&' : '?' ) + 'js_only=1';
					var x = document.getElementsByTagName("script")[0];
					x.parentNode.insertBefore(s, x);
				});
			});
			</script>
			
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/akismet/_inc/form.js?ver=3.0.4"></script>
<script type="text/javascript" src="http://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=201451"></script>
<script type="text/javascript" src="http://s.gravatar.com/js/gprofiles.js?ver=2014Decaa"></script>
<script type="text/javascript">
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/jetpack/modules/wpgroho.js?ver=4.0.1"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/themes/yoko-wpcom/js/navigation.js?ver=20140702"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-includes/js/comment-reply.min.js?ver=4.0.1"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/jetpack/_inc/postmessage.js?ver=3.3"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/jetpack/_inc/jquery.jetpack-resize.js?ver=3.3"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/jetpack/_inc/jquery.inview.js?ver=3.3"></script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/jetpack/modules/likes/queuehandler.js?ver=3.3"></script>
<script type="text/javascript">
/* <![CDATA[ */
var sharing_js_options = {"lang":"en","counts":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="http://enablingthefuture.org/wp-content/plugins/jetpack/modules/sharedaddy/sharing.js?ver=20140920"></script>
		<iframe src="http://widgets.wp.com/likes/master.html?ver=20141028#ver=20141028&amp;lang=en&amp;mp6=0" scrolling="no" id="likes-master" name="likes-master" style="display:none;"></iframe>
		<div id="likes-other-gravatars"><div class="likes-text"><span>%d</span> bloggers like this:</div><ul class="wpl-avatars sd-like-gravatars"></ul></div>
		
		<!--[if IE]>
		<script type="text/javascript">
		if ( 0 === window.location.hash.indexOf( '#comment-' ) ) {
			// window.location.reload() doesn't respect the Hash in IE
			window.location.hash = window.location.hash;
		}
		</script>
		<![endif]-->
		<script type="text/javascript">
			var comm_par_el = document.getElementById( 'comment_parent' ),
			    comm_par = (comm_par_el && comm_par_el.value) ? comm_par_el.value : '',
			    frame = document.getElementById( 'jetpack_remote_comment' ),
			    tellFrameNewParent;

			tellFrameNewParent = function() {
				if ( comm_par ) {
					frame.src = "http://jetpack.wordpress.com/jetpack-comment/?blogid=80229108&postid=819&comment_registration=0&require_name_email=1&stc_enabled=1&stb_enabled=1&show_avatars=1&avatar_default=identicon&greeting=Leave+a+Reply&greeting_reply=Leave+a+Reply+to+%25s&color_scheme=light&lang=en-US&jetpack_version=3.3&sig=b036a7cef106a24bd2bf15c8f324ca7d64b0168e#parent=http%3A%2F%2Fenablingthefuture.org%2Fget-involved%2F" + '&replytocom=' + parseInt( comm_par, 10 ).toString();
				} else {
					frame.src = "http://jetpack.wordpress.com/jetpack-comment/?blogid=80229108&postid=819&comment_registration=0&require_name_email=1&stc_enabled=1&stb_enabled=1&show_avatars=1&avatar_default=identicon&greeting=Leave+a+Reply&greeting_reply=Leave+a+Reply+to+%25s&color_scheme=light&lang=en-US&jetpack_version=3.3&sig=b036a7cef106a24bd2bf15c8f324ca7d64b0168e#parent=http%3A%2F%2Fenablingthefuture.org%2Fget-involved%2F";
				}
			};

	
			if ( 'undefined' !== typeof addComment ) {
				addComment._Jetpack_moveForm = addComment.moveForm;

				addComment.moveForm = function( commId, parentId, respondId, postId ) {
					var returnValue = addComment._Jetpack_moveForm( commId, parentId, respondId, postId ), cancelClick, cancel;

					if ( false === returnValue ) {
						cancel = document.getElementById( 'cancel-comment-reply-link' );
						cancelClick = cancel.onclick;
						cancel.onclick = function() {
							var cancelReturn = cancelClick.call( this );
							if ( false !== cancelReturn ) {
								return cancelReturn;
							}

							if ( !comm_par ) {
								return cancelReturn;
							}

							comm_par = 0;

							tellFrameNewParent();

							return cancelReturn;
						};
					}

					if ( comm_par == parentId ) {
						return returnValue;
					}

					comm_par = parentId;

					tellFrameNewParent();

					return returnValue;
				};
			}

	
			if ( window.postMessage ) {
				if ( document.addEventListener ) {
					window.addEventListener( 'message', function( event ) {
						if ( "http:\/\/jetpack.wordpress.com" !== event.origin ) {
							return;
						}

						jQuery( frame ).height( event.data );
					} );
				} else if ( document.attachEvent ) {
					window.attachEvent( 'message', function( event ) {
						if ( "http:\/\/jetpack.wordpress.com" !== event.origin ) {
							return;
						}

						jQuery( frame ).height( event.data );
					} );
				}
			}
		</script>

	
	<script src="http://stats.wp.com/e-201451.js" type="text/javascript"></script>
	<script type="text/javascript">
	st_go({v:'ext',j:'1:3.3',blog:'80229108',post:'819',tz:'0'});
	var load_cmc = function(){linktracker_init(80229108,819,2);};
	if ( typeof addLoadEvent != 'undefined' ) addLoadEvent(load_cmc);
	else load_cmc();
	</script><img id="wpstats" src="http://pixel.wp.com/g.gif?host=enablingthefuture.org&amp;rand=0.4131936435587704&amp;v=ext&amp;j=1%3A3.3&amp;blog=80229108&amp;post=819&amp;tz=0&amp;ref=http%3A//enablingthefuture.org/photo-galleries/" alt=""></div><!-- end page -->


<!-- Dynamic page generated in 0.449 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2014-12-18 11:47:52 -->

<!-- super cache --><div id="sharing_email" style="display: none;">
		<form action="/get-involved/" method="post">
			<label for="target_email">Send to Email Address</label>
			<input type="email" name="target_email" id="target_email" value="">

			
				<label for="source_name">Your Name</label>
				<input type="text" name="source_name" id="source_name" value="">

				<label for="source_email">Your Email Address</label>
				<input type="email" name="source_email" id="source_email" value="">

			
			
			<img style="float: right; display: none" class="loading" src="http://enablingthefuture.org/wp-content/plugins/jetpack/modules/sharedaddy/images/loading.gif" alt="loading" width="16" height="16">
			<input type="submit" value="Send Email" class="sharing_send">
			<a href="#cancel" class="sharing_cancel">Cancel</a>

			<div class="errors errors-1" style="display: none;">
				Post was not sent - check your email addresses!			</div>

			<div class="errors errors-2" style="display: none;">
				Email check failed, please try again			</div>

			<div class="errors errors-3" style="display: none;">
				Sorry, your blog cannot share posts by email.			</div>
		</form>
	</div><iframe id="rufous-sandbox" scrolling="no" frameborder="0" allowtransparency="true" style="display: none;"></iframe><script id="hiddenlpsubmitdiv" style="display: none;"></script><script>try{for(var lastpass_iter=0; lastpass_iter < document.forms.length; lastpass_iter++){ var lastpass_f = document.forms[lastpass_iter]; if(typeof(lastpass_f.lpsubmitorig2)=="undefined"){ lastpass_f.lpsubmitorig2 = lastpass_f.submit; lastpass_f.submit = function(){ var form=this; var customEvent = document.createEvent("Event"); customEvent.initEvent("lpCustomEvent", true, true); var d = document.getElementById("hiddenlpsubmitdiv"); if (d) {for(var i = 0; i < document.forms.length; i++){ if(document.forms[i]==form){ if (typeof(d.innerText) != 'undefined') { d.innerText=i; } else { d.textContent=i; } } } d.dispatchEvent(customEvent); }form.lpsubmitorig2(); } } }}catch(e){}</script></body></html>