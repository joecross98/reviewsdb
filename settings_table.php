<?php

	require_once('preheader.php'); // <-- this include file MUST go first before any HTML/output

	#the code for the class
	include ('ajaxCRUD.class.php'); // <-- this include file MUST go first before any HTML/output

    #this one line of code is how you implement the class
    ########################################################
    ##

?>


<?php
    
    $tblDemo4 = new ajaxCRUD("Preferred Setting", "settings", "idSettings");
    $tblDemo4->defineRelationship("PrinterID", "printers", "idPrinter", "CONCAT(Manufacturer, \" \", Model)");
    $tblDemo4->defineRelationship("HandID", "hands", "idHands", "Name");
    $tblDemo4->displayAs("PrinterID", "Printer");
    $tblDemo4->displayAs("HandID", "Hand");
    $tblDemo4->displayAs("LayerHeight", "Layer Height (mm)");
    $tblDemo4->displayAs("FillDensity", "Fill Density (%)");
    $tblDemo4->displayAs("PrinterSpeed", "Printer Speed (mm/s)");
    $tblDemo4->displayAs("AdhesionType", "Adhesion Type");
    $tblDemo4->displayAs("ShellThickness", "Shell Thickness (mm)");
    $tblDemo4->displayAs("EnableRetraction", "Enable Retraction");
    $tblDemo4->defineCheckbox("EnableRetraction");
    $tblDemo4->displayAs("BottomTopThickness", "Bottom/Top Thickness (mm)");
    $tblDemo4->displayAs("SupportType", "Support Type");
    $tblDemo4->omitFieldCompletely("idSettings");
    $tblDemo4->setFileUpload("Photo", "Pics/", "Pics/");
    $tblDemo4->disallowDelete();
    $tblDemo4->disallowAdd();
    $tblDemo4->addOrderBy("ORDER BY PrinterID ASC ");
    $tblDemo4->addTableBorder();
    $tblDemo4->addAjaxFilterBoxAllFields();
    $tblDemo4->showTable();
   
?>

<script type="text/javascript">$('#add_form_settings').slideDown('fast'); x = document.getElementById('add_form_settings'); t = setTimeout('x.scrollIntoView(false)', 200);</script>
			