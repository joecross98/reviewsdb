-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2015 at 10:53 AM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a4823675_enable`
--

-- --------------------------------------------------------

--
-- Table structure for table `hands`
--

CREATE TABLE `hands` (
  `idHands` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Link` varchar(2083) DEFAULT NULL,
  `TaskSpecific` tinyint(1) NOT NULL DEFAULT '0',
  `Contact` varchar(255) DEFAULT NULL,
  `Notes` varchar(2083) DEFAULT NULL,
  PRIMARY KEY (`idHands`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `hands`
--

INSERT INTO `hands` VALUES(1, 'Raptor', 'http://enablingthefuture.org/upper-limb-prosthetics/the-raptor-hand/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(2, 'Cyborg Beast', 'http://enablingthefuture.org/upper-limb-prosthetics/cyborg-beast/', 0, 'Mark Petrykowski', NULL);
INSERT INTO `hands` VALUES(3, 'Talon', 'http://enablingthefuture.org/upper-limb-prosthetics/talon-hand/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(4, 'Odysseus', 'http://enablingthefuture.org/upper-limb-prosthetics/odysseus-hand/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(5, 'Second Degree', 'http://enablingthefuture.org/upper-limb-prosthetics/second-degree/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(6, 'Owen Replacement Finger', 'http://enablingthefuture.org/upper-limb-prosthetics/the-owen-replacement-finger/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(7, 'Limbitless Arm', 'http://enablingthefuture.org/upper-limb-prosthetics/the-limbitless-arm/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(8, 'RIT Arm', 'http://enablingthefuture.org/upper-limb-prosthetics/rit-arm/', 0, NULL, NULL);
INSERT INTO `hands` VALUES(14, 'Baseball Glove', '', 1, 'Bob Roth', 'leather cone construction');
INSERT INTO `hands` VALUES(15, 'Violin Assistance', '', 1, 'Bob Roth', 'Already Completed by Creighton University');
INSERT INTO `hands` VALUES(16, 'Writing Device', '', 1, 'Vikas Kumar Singh', NULL);
INSERT INTO `hands` VALUES(17, 'Batting Device', '', 1, 'Bob Roth', NULL);
INSERT INTO `hands` VALUES(18, 'Swimming Device', '', 1, 'Bob Roth', 'Already Completed by Creighton University');
INSERT INTO `hands` VALUES(19, 'Bike Riding', '', 1, 'Bruce Newell', 'Already Completed by Creighton University');
INSERT INTO `hands` VALUES(20, 'Rowing (holding a oar)', '', 1, 'Alexander Manes:zander0411@gmail.com ', NULL);
INSERT INTO `hands` VALUES(21, 'Exoskeletal Device for Muscle Deficient Patie', '', 1, 'Mohit Chaudhary', 'Pneumatic Device utilizing rolling diaphragms in pistons');

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `idMaterial` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `diameter` float NOT NULL,
  PRIMARY KEY (`idMaterial`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` VALUES(1, 'PLA', 1.75);
INSERT INTO `materials` VALUES(2, 'ABS', 1.75);
INSERT INTO `materials` VALUES(4, 'PLA', 3);
INSERT INTO `materials` VALUES(5, 'ABS', 3);

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

CREATE TABLE `printers` (
  `Manufacturer` varchar(45) NOT NULL,
  `Model` varchar(45) NOT NULL,
  `idPrinter` int(11) NOT NULL AUTO_INCREMENT,
  `URL` varchar(2083) DEFAULT NULL,
  PRIMARY KEY (`Manufacturer`,`Model`),
  UNIQUE KEY `idprinters_UNIQUE` (`idPrinter`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `printers`
--

INSERT INTO `printers` VALUES('Cubify', 'Cube', 17, 'http://cubify.com/en/Products/Printers');
INSERT INTO `printers` VALUES('Cubify', 'CubeX', 16, 'http://cubify.com/en/Products/Legacy');
INSERT INTO `printers` VALUES('Cubify', 'Duo', 18, 'http://cubify.com/en/Products/Printers');
INSERT INTO `printers` VALUES('FlashForge', 'Creator Dual ', 12, 'http://www.flashforge-usa.com/shop/3d-printers.html');
INSERT INTO `printers` VALUES('FlashForge', 'Creator Pro Dual', 13, 'http://www.flashforge-usa.com/shop/3d-printers.html');
INSERT INTO `printers` VALUES('FlashForge', 'Creator X Dual ', 14, 'http://www.flashforge-usa.com/shop/3d-printers.html');
INSERT INTO `printers` VALUES('FlashForge', 'Dreamer', 11, 'http://www.flashforge-usa.com/shop/3d-printers.html');
INSERT INTO `printers` VALUES('Inventapart', 'RigidBot', 40, 'http://www.inventapart.com/rigidbot.php');
INSERT INTO `printers` VALUES('Inventapart', 'RigidBot big', 41, 'http://www.inventapart.com/rigidbot.php');
INSERT INTO `printers` VALUES('Makerbot', 'Replicator (5th generation)', 28, 'http://store.makerbot.com/replicator');
INSERT INTO `printers` VALUES('Makerbot', 'Replicator 2X', 30, 'http://store.makerbot.com/replicator2x');
INSERT INTO `printers` VALUES('Makerbot', 'Replicator MINI', 27, 'http://store.makerbot.com/replicator-mini');
INSERT INTO `printers` VALUES('Makerbot', 'Replicator2', 29, 'http://store.makerbot.com/replicator2');
INSERT INTO `printers` VALUES('Printrbot', 'Go v2', 25, 'http://printrbot.com/product-category/3d-printers/');
INSERT INTO `printers` VALUES('Printrbot', 'Go v2 Medium', 24, 'http://printrbot.com/product-category/3d-printers/');
INSERT INTO `printers` VALUES('Printrbot', 'LC from kit', 26, 'http://printrbot.com/product-category/3d-printers/');
INSERT INTO `printers` VALUES('Printrbot', 'Simple Metal', 23, 'http://printrbot.com/product-category/3d-printers/');
INSERT INTO `printers` VALUES('RepRap', 'Huxley', 7, 'http://reprap.org/wiki/RepRapPro_Huxley');
INSERT INTO `printers` VALUES('RepRap', 'Kossel Mini', 1, 'http://reprap.org/wiki/Kossel');
INSERT INTO `printers` VALUES('RepRap', 'Mendel', 4, 'http://reprap.org/wiki/Mendel');
INSERT INTO `printers` VALUES('RepRap', 'MendelMax 1.5', 5, 'http://reprap.org/wiki/MendelMax');
INSERT INTO `printers` VALUES('RepRap', 'Prusa i3', 3, 'http://reprap.org/wiki/Prusa');
INSERT INTO `printers` VALUES('RepRap', 'Prusa Mendel i2', 2, 'http://reprap.org/wiki/Prusa');
INSERT INTO `printers` VALUES('RepRap', 'TriColor', 8, 'http://reprap.org/wiki/RepRapPro_Tricolour');
INSERT INTO `printers` VALUES('RepRap', 'Wallace', 6, 'http://reprap.org/wiki/Wallace');
INSERT INTO `printers` VALUES('Robo3D', 'R1 Robo3D', 15, 'http://www.robo3dprinter.com/products/robo-3d-abs-model-fully-assembled');
INSERT INTO `printers` VALUES('SeeMeCNC', 'DropLit DIY', 39, 'http://seemecnc.com/collections/3d-printers/products/droplit-diy-kit');
INSERT INTO `printers` VALUES('SeeMeCNC', 'Orion Delta', 37, 'http://seemecnc.com/collections/3d-printers/products/orion-delta-3d-printer');
INSERT INTO `printers` VALUES('SeeMeCNC', 'Rostock MAX v2', 38, 'http://seemecnc.com/collections/3d-printers/products/rostock-max-complete-kit');
INSERT INTO `printers` VALUES('Solidoodle', '4', 36, 'http://www.solidoodle.com/Solidoodle4');
INSERT INTO `printers` VALUES('Solidoodle', 'Press', 33, 'http://www.solidoodle.com/Press');
INSERT INTO `printers` VALUES('Solidoodle', 'Workbench', 34, 'http://www.solidoodle.com/workbench');
INSERT INTO `printers` VALUES('Solidoodle', 'Workbench Apprentice', 35, 'http://www.solidoodle.com/workbench-apprentice');
INSERT INTO `printers` VALUES('Stratasys', 'uPrint SE', 42, 'http://www.stratasys.com/3d-printers/idea-series/uprint-se');
INSERT INTO `printers` VALUES('Ultimaker', 'Original', 31, 'https://www.ultimaker.com/pages/our-printers/ultimaker-original');
INSERT INTO `printers` VALUES('Ultimaker', 'Ultimaker 2', 32, 'https://www.ultimaker.com/pages/our-printers/ultimaker-2');
INSERT INTO `printers` VALUES('Up', 'Up Box', 22, 'http://www.up3dusa.com/');
INSERT INTO `printers` VALUES('Up', 'Up Mini', 19, 'http://www.up3dusa.com/#!mini/c14nt');
INSERT INTO `printers` VALUES('Up', 'Up Plus', 20, 'http://www.up3dusa.com/#!plus/c2nh');
INSERT INTO `printers` VALUES('Up', 'Up Plus 2', 21, 'http://www.up3dusa.com/#!plus/cyam');
INSERT INTO `printers` VALUES('XYZ Printing', 'Da Vinci 1.0', 9, 'http://us.xyzprinting.com/product');
INSERT INTO `printers` VALUES('XYZ Printing', 'Da Vinci 2.0 Duo', 10, 'http://us.xyzprinting.com/product');
INSERT INTO `printers` VALUES('Boots Industries', 'BI V2.5 3D', 45, 'http://bootsindustries.com/shop/biv2/');
INSERT INTO `printers` VALUES('Wanhao', 'Duplicator 4x', 46, 'http://wanhaousa.com/collections/3d-printers/products/duplicator-4s-steel-exoframe\n\nOR\n\nhttp://item.taobao.com/item.htm?spm=a230r.1.14.1.nJf1Mi&id=35893951111&ns=1&abbucket=12#detail');

-- --------------------------------------------------------

--
-- Table structure for table `printer_material_rel`
--

CREATE TABLE `printer_material_rel` (
  `printer` int(11) NOT NULL,
  `material` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `printer_material_rel`
--

INSERT INTO `printer_material_rel` VALUES(12, 1);
INSERT INTO `printer_material_rel` VALUES(12, 2);
INSERT INTO `printer_material_rel` VALUES(13, 1);
INSERT INTO `printer_material_rel` VALUES(13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `idReviews` int(11) NOT NULL AUTO_INCREMENT,
  `PrinterID` int(11) NOT NULL,
  `Commentor` varchar(100) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `ColorCode` varchar(45) DEFAULT NULL,
  `Advantages` tinytext,
  `Disadvantages` tinytext,
  `Notes` text,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`idReviews`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` VALUES(7, 9, 'David Maulik', 499, 'Unsuccessful', 'Low-cost machine', 'Low quality prints, Incredibly difficult to level the bed, proprietary fillament costs approximately 30% more than \\"standard\\", only prints in ABS', 'This machine advertises 100 micron resolution (0.1 mm) but I have yet to achieve any results at this resolution that are acceptable.', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(8, 9, 'Tom Burbridge', 529, 'Successful with hard provisions', 'Low Cost', 'Machine requires modification to make this an effective printer for prosthetics.', 'For this printer to be used for prosthetics: 1. Replace the 35w extruder heater with a 40w. 2. Hack the filament cartridge and use high quality filament. 3. Set the quality to 90% solid. 4. Use the hand 3ds files uploaded to thingiverse.', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(9, 11, 'David Maulik', 1299, 'Successful with hard provisions', 'Dual extruder (can print soluable support material), consistant print results, can easily print ABS and PLA', 'I believe you have to use \\"FlashPrint\\" software to send files to the machine but the machine does use standard gcode files for printing.', 'I have not had a chance to print any of the e-nable hands on this printer so I will wait to judge the suitability until I have done so. (I would be suprised if it doesn''t work great)', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(10, 15, 'Aaron Brown', 799, 'Successfully used.  Well-suited to beginners.', 'Cost, build area 10x9x8, autolevel, heated build plate, multi-material.', 'No enclosure. Open sourced software learning curve.', 'I have printed many hands on two of these machines. The usual printing tricks still apply for good prints, but if cost is a factor, it is a very solid option.', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(11, 16, 'Bruce Newell', 2600, 'Successfully used.  Well-suited to beginners.', 'good printer most of the time', 'no hot plate, some times failure', 'The Cubify web site lists this as a legacy printer.  Limited support and supplies.  Same with Cube2 printer.  Replaced by newer Cube3 line of printers.', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(12, 31, 'Sandra Dermisek', 956, 'Successfully used.  Well-suited to beginners.', '\\"* dual extrusion available\n* a heated bed available\n* very complete online support\n* on the ultimaker forum you can find answers on many subjects\\"', '\\"* not all filament spools fit on the machine it self, but you can find on youmagine.com many solutions for that.\n* In the beginning I had the problem that the bed didn''t stay on it''s place. The springs under the screws where the bed is attached are not ', '\\"It''s very important to calibrate you machine very well. The bed must be really level to make good prints. In Cura the is a bedleveling wizard that makes this much easier.\nI have only printed PLA and flexible PLA yet and that works really good.\nI also have the dual extrusion and it makes really nice 2 colored parts.\nIn short I am really happy with my Ultimaker Original\\"', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(13, 46, 'Jason Bryant', 800, 'Successful with easy provisions', 'It included so many spare screws and electrical parts, I thought the assembly was going to be much harder than it actually was. :)', 'I''ve had a few issues with slopes. Printing the dovetail cap produces a bit of a mess at the edges. It also needs added supports to bridge the big gaps in the gauntlet.', 'I got it off of Taobao.  Per Laird, it''s basically a copy of Makerbot''s Replicator 2X. On his advice, I''m using Makerbot''s desktop for actually sending the stl files to the machine.  The Raptor finger that I printed out first moved around, so I tried again with a raft. That also moved around. So I followed the instructions in the manual and put some masking tape on the platform (masking tape was included). That worked great!\n\nThe printer came with two manuals, one in Chinese and one in English.  The English one has a few errors, but nothing problematic.  The only real assembly was for the bit plastic case that goes on top. That took an hour or two.  The Taobao page actually had a link to a video showing how to assemble it, Which was in Chinese.\nI might have been able to do it faster if it had been in English, but it was pretty clear what he was doing.', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(14, 32, 'Palmers 4', 0, 'Successfully used.  Well-suited to beginners.', '', '', 'We have not printed in PLA, ABS, and PET  for hands.  Not sure the PET is the right material.  Also been playing with Flex PLA which printed fairly well, but the Nina Flex not so well.', '0000-00-00 00:00:00');
INSERT INTO `reviews` VALUES(47, 29, 'Andreas Bastian', 2200, 'Successfully used.  Well-suited to beginners.', 'Quiet, reliable, large build volume', 'Once damaged, quite difficult to fix.\r\nLimited materials', 'I\\''m a big fan and have run my machine every day for the past 18 months.', '2015-01-19 17:31:23');
INSERT INTO `reviews` VALUES(57, 17, '', 0, 'Unsuccessful', '', '', '', '2015-01-21 10:43:11');
INSERT INTO `reviews` VALUES(58, 29, 'Bret Wortman', 2000, 'Successful with easy provisions', 'PLA can be printed almost out of the box. With simple mods, can also print ABS, NinjaFlex, T-Glase, and other materials as well. There\\''s a large, active, and very helpful Google Groups community of users that got me through the early setup and some novic', 'Cost. Support from Makerbot is almost nonexistent (good thing plenty of other companies have jumped in to fill the void). Some extensions require minor disassembly.', 'I print PLA using Aquanet on an aftermarket glass plate. I print ABS using an aftermarket HBP and have enclosed the printer (except the top) using a plexiglass side-and-door kit. I\\''ve also upgraded the extruder to use all aluminum parts.', '2015-01-21 11:54:34');